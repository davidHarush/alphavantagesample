# README #

## alphavantage - sample
Sample app - showing data lists with MotionLayout and more...

### Used technologies:
- Written in Kotlin
- Kotlin coroutines + suspend function
- Android Architecture components: ViewModel, Livedata.
- Single activity architecture with Navigation component
- Retrofit2.
- Coil: Image loading library backed by Kotlin Coroutines.
- MotionLayout with MotionScene


### How do I get set up? ###

* Simple clone like all others git repository
* [Apk For Download](https://bitbucket.org/davidHarush/alphavantagesample/raw/88e5b150e69ee006668382eddcf9e558b2a717a2/app.apk) 