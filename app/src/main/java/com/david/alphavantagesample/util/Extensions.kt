package com.david.alphavantagesample.util


import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import android.widget.Toast.makeText
import com.david.alphavantagesample.App


fun getAppContext() =
    App.applicationContext()

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}


fun doWithDelay(time: Long, action: () -> Unit) {
    Handler(Looper.getMainLooper()).postDelayed({
        action()
    }, time)
}

fun showToast(msg: String) {
    makeText(getAppContext(), msg, Toast.LENGTH_LONG).show()
}

fun View.fadeInAnimate(duration: Long = 1500) {
    clearAnimation()
    alpha = 0f
    visible()
    animate().setDuration(duration).alphaBy(1f).start()
}

