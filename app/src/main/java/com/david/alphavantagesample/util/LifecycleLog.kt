package com.david.alphavantagesample.util


import android.util.Log

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 *  log Lifecycle objects
 */
class LifecycleLog(private val lifecycle: Lifecycle, var  LifecycleOwnerName: String) :  LifecycleObserver {

    companion object {
        private val TAG = LifecycleLog::class.java.simpleName
    }

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun logOnResume() {
        Log.i(
            TAG,
            "$LifecycleOwnerName onResume"
        )
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun logOnPause() {
        Log.i(
            TAG,
            "$LifecycleOwnerName OnPause"
        )
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun logOnStop() {
        Log.i(
            TAG,
            "$LifecycleOwnerName OnStop"
        )
    }

}
