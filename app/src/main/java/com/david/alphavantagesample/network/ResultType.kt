package com.david.alphavantagesample.network

/**
 * Created by David Harush
 * On 10/08/2020.
 */
enum class ResultType {
    SUCCESS,
    FAIL,
}