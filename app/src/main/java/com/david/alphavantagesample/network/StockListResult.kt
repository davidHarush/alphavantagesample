package com.david.alphavantagesample.network

import com.david.alphavantagesample.network.entities.BankItem


data class StockListResult(
    val data: ArrayList<BankItem>,
    val resultType: ResultType = ResultType.SUCCESS,
    val msg: String
)

