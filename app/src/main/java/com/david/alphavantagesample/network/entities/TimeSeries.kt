package com.david.alphavantagesample.network.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TimeSeries(
    @SerializedName("1. open")
    @Expose
    val open: String? = null,

    @SerializedName("2. high")
    @Expose
    val high: String? = null,

    @SerializedName("3. low")
    @Expose
    val low: String? = null,

    @SerializedName("4. close")
    @Expose
    val close: String? = null,

    @SerializedName("5. volume")
    @Expose
    val volume: String? = null
) : Parcelable