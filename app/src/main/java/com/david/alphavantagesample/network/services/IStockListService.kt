package com.david.alphavantagesample.network.services

import com.david.alphavantagesample.network.entities.BankItem
import retrofit2.Response
import retrofit2.http.GET

interface IStockListService {
    // https://pastebin.com/raw/AW8NT14G


    @GET("/raw/AW8NT14G")
    suspend fun getData(): Response<ArrayList<BankItem>>

    companion object {
        const val baseUrl = " https://pastebin.com"
    }
}