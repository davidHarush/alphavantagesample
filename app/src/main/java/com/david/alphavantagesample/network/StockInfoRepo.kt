package com.david.alphavantagesample.network

import com.david.alphavantagesample.network.entities.MetaData
import com.david.alphavantagesample.network.entities.TimeSeries
import com.david.alphavantagesample.network.services.IAlphaVantageService
import com.google.gson.Gson
import com.google.gson.JsonElement
import org.json.JSONObject
import retrofit2.Response
import java.util.*


object StockInfoRepo {

    public fun getBaseUrl(): String =
        IAlphaVantageService.baseUrl


    public suspend fun getData( interval: String, symbol: String): StockInfoResult {


        val response: Response<JsonElement> =
            ServiceRetrofit.create(serviceClass = IAlphaVantageService::class.java)
                .getData(interval = interval, symbol = symbol)

        if (!response.isSuccessful) {
            return StockInfoResult(
                resultType = ResultType.FAIL,
                data = LinkedList(),
                metadata = MetaData(),
                msg = "Err Code: ${response.code()}"
            )
        }


        val jsonResult = JSONObject(response.body()!!.asJsonObject.toString())
        val keys: Iterator<*> = jsonResult.keys()
        var metadata = MetaData()
        var data = LinkedList<Pair<String, TimeSeries>>()
        val gson = Gson()

        while (keys.hasNext()) {
            val key = keys.next() as String
            if (key == "Meta Data") {
                metadata =
                    gson.fromJson(jsonResult.getJSONObject(key).toString(), MetaData::class.java)
            }

            if (key.startsWith("Time Series")) {
                val timeSeriesKeys: Iterator<*> = jsonResult.getJSONObject(key).keys()
                while (timeSeriesKeys.hasNext()) {
                    val timeKey = timeSeriesKeys.next() as String
                    var time = timeKey.trim().split(" ")[1]
                    data.add(
                        Pair(
                            first = time,
                            second = gson.fromJson(
                                jsonResult.getJSONObject(key).getJSONObject(timeKey).toString(),
                                TimeSeries::class.java
                            )
                        )
                    )

                }
            }

        }

        return if (data.isEmpty()) {
            StockInfoResult(
                resultType = ResultType.FAIL,
                data = LinkedList(),
                metadata = MetaData(),
                msg = "Empty Data"
            )
        } else
            StockInfoResult(
                resultType = ResultType.SUCCESS,
                data = data,
                metadata = metadata,
                msg = "OK"
            )

    }
}
