package com.david.alphavantagesample.network.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BankItem(
    @SerializedName("name")
    @Expose
    val name: String? = null,

    @SerializedName("stk")
    @Expose
    val stk: String? = null,

    @SerializedName("img")
    @Expose
    val img: String? = null,

    @SerializedName("priority")
    @Expose
    val priority: String? = null
) : Parcelable