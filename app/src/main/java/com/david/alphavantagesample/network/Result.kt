package com.david.alphavantagesample.network

import com.david.alphavantagesample.network.entities.MetaData
import com.david.alphavantagesample.network.entities.TimeSeries
import java.util.*


data class Result(
    val data: ArrayList<Pair<String,TimeSeries>>,
    val metadata: MetaData,
    val resultType: ResultType = ResultType.SUCCESS,
    val msg: String
)

