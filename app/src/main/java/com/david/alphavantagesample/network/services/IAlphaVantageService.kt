package com.david.alphavantagesample.network.services

import com.google.gson.JsonElement
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IAlphaVantageService {
    // https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=JPM&interval=1min&apikey=Z8EW6CI3PHR9SUTK


    @GET("/query?function=TIME_SERIES_INTRADAY&apikey=Z8EW6CI3PHR9SUTK")
    suspend fun getData(@Query("interval") interval: String, @Query("symbol") symbol: String): Response<JsonElement>

    companion object {
        const val baseUrl = "http://www.alphavantage.co"
    }
}