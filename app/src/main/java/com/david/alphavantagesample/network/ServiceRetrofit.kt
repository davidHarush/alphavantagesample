package com.david.alphavantagesample.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * singleton object to configure Retrofit
 */
object ServiceRetrofit {

    private val builder = Retrofit.Builder()
        .baseUrl(StockInfoRepo.getBaseUrl())
        .addConverterFactory(GsonConverterFactory.create())
    private val loggingInterceptor = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)

    private val httpClient = OkHttpClient.Builder()

    fun <S> create(serviceClass: Class<S>): S {
        if (!httpClient.interceptors()
                .contains(loggingInterceptor)
        ) {
            httpClient.addInterceptor(loggingInterceptor)
            builder.client(httpClient.build())
        }
        return builder.build().create(serviceClass)
    }

    fun <S> create(baseUrl: String, serviceClass: Class<S>): S {
        httpClient.addInterceptor(loggingInterceptor)
        val customBuilder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
        customBuilder.client(httpClient.build())
        return customBuilder.build().create(serviceClass)
    }
}