package com.david.alphavantagesample.network

import com.david.alphavantagesample.network.entities.BankItem
import com.david.alphavantagesample.network.services.IStockListService
import retrofit2.Response


object StockListRepo {

    private fun getBaseUrl(): String =
        IStockListService.baseUrl


    public suspend fun getData(): StockListResult {
        val response: Response<ArrayList<BankItem>> =
            ServiceRetrofit.create(baseUrl = getBaseUrl(), serviceClass = IStockListService::class.java).getData()

        if (!response.isSuccessful) {
            return StockListResult(
                resultType = ResultType.FAIL,
                data = ArrayList(),
                msg = "Err Code: ${response.code()}"
            )
        }

        val data = response.body()?.let {
            response.body()
        } ?: run {
            ArrayList<BankItem>()
        }

        return StockListResult(
                resultType = if (data.isEmpty()) ResultType.FAIL else ResultType.SUCCESS,
                data = data,
                msg = if (data.isEmpty()) "ERR" else "OK"
            )
        }

}
