package com.david.alphavantagesample.network

import com.david.alphavantagesample.network.entities.MetaData
import com.david.alphavantagesample.network.entities.TimeSeries
import com.google.gson.Gson
import com.google.gson.JsonElement
import org.json.JSONObject
import retrofit2.Response


object Repo {

    public fun getBaseUrl(): String =
        IAlphaVantageService.baseUrl


    public suspend fun getData(): Result {


        val response: Response<JsonElement> =
            ServiceRetrofit.create(serviceClass = IAlphaVantageService::class.java).getData()

        if (!response.isSuccessful) {
            return Result(
                resultType = ResultType.FAIL,
                data = ArrayList(),
                metadata = MetaData(),
                msg = "Err Code: ${response.code()}"
            )
        }


        val jsonResult = JSONObject(response.body()!!.asJsonObject.toString())
        val keys: Iterator<*> = jsonResult.keys()
        var metadata = MetaData()
        var data = ArrayList<Pair<String, TimeSeries>>()
        val gson = Gson()

        while (keys.hasNext()) {
            val key = keys.next() as String
            if (key == "Meta Data") {
                metadata =
                    gson.fromJson(jsonResult.getJSONObject(key).toString(), MetaData::class.java)
            }

            if (key.startsWith("Time Series")) {
                val timeSeriesKeys: Iterator<*> = jsonResult.getJSONObject(key).keys()
                while (timeSeriesKeys.hasNext()) {
                    val time = timeSeriesKeys.next() as String
                    data.add(
                        Pair(
                            first = time,
                            second = gson.fromJson(
                                jsonResult.getJSONObject(key).getJSONObject(time).toString(),
                                TimeSeries::class.java
                            )
                        )
                    )

                }
            }

        }

        return Result(
            resultType = ResultType.SUCCESS,
            data = data,
            metadata = metadata,
            msg = "OK"
        )

    }
}
