package com.david.alphavantagesample.network.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MetaData(
    @SerializedName("1. Information")
    @Expose
    val information: String? = null,

    @SerializedName("2. Symbol")
    @Expose
    val symbol: String? = null,

    @SerializedName("3. Last Refreshed")
    @Expose
    val videoId: String? = null,

    @SerializedName("4. Interval")
    @Expose
    val interval: String? = null,

    @SerializedName("5. Output Size")
    @Expose
    val outputSize: String? = null,

    @SerializedName("6. Time Zone")
    @Expose
    val timeZone: String? = null
) : Parcelable