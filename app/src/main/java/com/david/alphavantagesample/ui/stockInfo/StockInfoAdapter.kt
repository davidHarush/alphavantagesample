package com.david.alphavantagesample.ui.stockInfo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.david.alphavantagesample.R
import com.david.alphavantagesample.base.BaseHolder
import com.david.alphavantagesample.network.entities.TimeSeries
import com.david.alphavantagesample.util.getAppContext


class StockInfoAdapter(
    private val data: ArrayList<Pair<String, TimeSeries>>
) : RecyclerView.Adapter<StockInfoAdapter.DataViewHolder>() {

    private val mInflater :LayoutInflater =  LayoutInflater.from(getAppContext())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        return DataViewHolder(mInflater.inflate(R.layout.time_serial_item, parent, false))
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
      holder.onBind(data[position])
    }

    inner class DataViewHolder(itemView: View) : BaseHolder(itemView) {
        private val mTitle: TextView
        private val mLow: TextView
        private val mHigh: TextView
        private val mOpen: TextView
        private val mClose: TextView

        init {
            itemView.setOnClickListener(this)
            mTitle = itemView.findViewById(R.id.title)
            mLow = itemView.findViewById(R.id.low)
            mHigh = itemView.findViewById(R.id.high)
            mOpen = itemView.findViewById(R.id.open)
            mClose = itemView.findViewById(R.id.close)
        }

        override fun onBind(dataItem: Any) {
            val item = dataItem as Pair<String, TimeSeries>
            itemView.tag = item
            mTitle.text = item.first
            mLow.text ="Low : ${item.second.low}"
            mHigh.text = "High : ${item.second.high}"
            mOpen.text = "Open : ${item.second.open}"
            mClose.text = "Close : ${item.second.close}"

        }

        override fun onClick(v: View?) {
        }
    }

    interface CallBack {
//        fun onMovieClick(movie :YouTubeListItem)
    }

}