package com.david.alphavantagesample.ui

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.david.alphavantagesample.R
import com.david.alphavantagesample.base.BaseActivity
import com.david.alphavantagesample.ui.home.HomeViewModel
import com.david.alphavantagesample.ui.stockInfo.SelectedStockViewModel
import com.david.alphavantagesample.util.gone
import com.david.alphavantagesample.util.showToast
import com.david.alphavantagesample.util.visible
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : BaseActivity() {
    override fun getActivityName(): String = MainActivity::class.java.simpleName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        ViewModelProvider(this)[SelectedStockViewModel::class.java]
            .let { viewModel ->
                viewModel.getLoading()
                    .observe(this@MainActivity,
                        Observer {
                            loading(it)
                        })

                viewModel.getErr()
                    .observe(this@MainActivity,
                        Observer<String> { msg ->
                            if(msg.isNotEmpty()) {
                                showErr(msg)
                            }
                        })
            }

        ViewModelProvider(this)[HomeViewModel::class.java]
            .let { viewModel ->
                viewModel.start()
                viewModel.getLoading()
                    .observe(this@MainActivity,
                        Observer {
                            loading(it)
                        }
                    )
                viewModel.getErr()
                    .observe(this@MainActivity,
                        Observer<String> { msg ->
                            if(msg.isNotEmpty()) {
                                showErr(msg)
                            }
                        }
                    )
            }

    }

    private fun loading(loading: Boolean) {
        when (loading) {
            true -> {
                err.gone()
                progressBar.visible()
            }
            false -> {
                progressBar.gone()
            }
        }

    }

    private fun showErr(msg: String) {
        showToast(msg)
        progressBar.gone()
        err.visible()

    }

    override fun onBackPressed() {
        err.gone()
        super.onBackPressed()
    }
}