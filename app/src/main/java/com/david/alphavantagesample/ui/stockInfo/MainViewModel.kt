package com.david.alphavantagesample.ui.stockInfo

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.david.alphavantagesample.base.BaseViewModel
import com.david.alphavantagesample.network.Repo
import com.david.alphavantagesample.network.Result
import com.david.alphavantagesample.network.ResultType
import kotlinx.coroutines.launch


class MainViewModel(application: Application) : BaseViewModel(application) {

    private val loading = MutableLiveData<Boolean>(true)
    private val dataNews = MutableLiveData<Result>()
    private val err = MutableLiveData<String>()


    fun start() {

        startLoading()
        iOScope.launch {
            startLoading()
            val response: Result = Repo.getData()
            if (response.resultType == ResultType.SUCCESS) {
                dataNews.postValue(response)
            } else {
                err.postValue("err: ${response.msg}")
            }
            endLoading()

        }

    }

    private fun startLoading() {
        loading.postValue(true)
    }

    private fun endLoading() {
        loading.postValue(false)
    }


    fun getErr() = err
    fun getLoading() = loading
    fun getData() = dataNews


}