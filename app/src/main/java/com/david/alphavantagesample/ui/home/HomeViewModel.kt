package com.david.alphavantagesample.ui.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.david.alphavantagesample.base.BaseViewModel
import com.david.alphavantagesample.network.ResultType
import com.david.alphavantagesample.network.StockListRepo
import com.david.alphavantagesample.network.StockListResult
import kotlinx.coroutines.launch


class HomeViewModel(application: Application) : BaseViewModel(application) {

    private val loading = MutableLiveData<Boolean>(true)
    private val err = MutableLiveData<String>()
    private val stockList = MutableLiveData<StockListResult>()


    fun getErr() = err
    fun getLoading() = loading
    fun getStockList() = stockList


    fun start() {

        startLoading()
        iOScope.launch {
            startLoading()
            val response: StockListResult = StockListRepo.getData()
            if (response.resultType == ResultType.SUCCESS) {
                stockList.postValue(response)
                err.postValue("")
            } else {
                err.postValue("err: ${response.msg}")
            }
            endLoading()

        }
    }

    private fun startLoading() {
        loading.postValue(true)
    }

    private fun endLoading() {
        loading.postValue(false)
    }

}