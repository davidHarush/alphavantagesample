package com.david.alphavantagesample.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.david.alphavantagesample.R
import com.david.alphavantagesample.base.BaseFragment
import com.david.alphavantagesample.network.entities.BankItem
import com.david.alphavantagesample.ui.stockInfo.SelectedStockViewModel
import com.david.alphavantagesample.util.visible
import kotlinx.android.synthetic.main.stock_info_fragment.*

class HomeFragment : BaseFragment(R.layout.home_fragment), HomeAdapter.CallBack {


    companion object {
        fun newInstance() =
            HomeFragment()
    }

    private lateinit var homeViewModel: HomeViewModel


    override fun getFragmentName(): String = HomeFragment::class.java.simpleName

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let { fragmentActivity ->
            if (!::homeViewModel.isInitialized) {
                homeViewModel = ViewModelProvider(fragmentActivity)[HomeViewModel::class.java]

            }
            setObserves(fragmentActivity)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }


    private fun setObserves(fragmentActivity: FragmentActivity) {
        homeViewModel.getStockList()
            .observe(fragmentActivity,
                Observer { result ->
                    recyclerView.adapter =
                        HomeAdapter(
                            data = result.data,
                            callBack = this@HomeFragment

                        )
                    recyclerView.layoutManager =
                        GridLayoutManager(context, 2)
                    recyclerView.visible()


                })
    }

    override fun onItemClick(bankItem: BankItem) {
        activity?.let{fragmentActivity ->
            ViewModelProvider(fragmentActivity )[SelectedStockViewModel::class.java]
                .let { viewModel ->
                    viewModel.setSelectedBank(bankItem)
                    findNavController().navigate(R.id.action_HomeFragment_to_StockInfoFragment)

                }
        }

    }

}