package com.david.alphavantagesample.ui.stockInfo

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.david.alphavantagesample.base.BaseViewModel
import com.david.alphavantagesample.network.ResultType
import com.david.alphavantagesample.network.StockInfoRepo
import com.david.alphavantagesample.network.StockInfoResult
import com.david.alphavantagesample.network.entities.BankItem
import com.david.alphavantagesample.util.showToast
import kotlinx.coroutines.launch


class SelectedStockViewModel(application: Application) : BaseViewModel(application) {

    private val loading = MutableLiveData<Boolean>(true)
    private var selectedStockInfo = MutableLiveData<StockInfoResult>()
    private val err = MutableLiveData<String>()

    companion object {
        public const val Interval_1min = "1min"
        public const val Interval_5min = "5min"
        public const val Interval_15min = "15min"
        public const val Interval_30min = "30min"
        public const val Interval_60min = "60min"
    }


    private var infoInterval: String = "1min"
    private var selectedBank: BankItem = BankItem()

    fun setInterval(interval: String) {
        infoInterval = interval
    }

    fun setSelectedBank(bank: BankItem) {
        selectedBank = bank
        selectedBank.name?.let { showToast(it) }
        selectedStockInfo = MutableLiveData()
    }

    fun getErr() = err
    fun getLoading() = loading
    fun getStockInfo() = selectedStockInfo


    fun runStockInfo() {

        startLoading()
        iOScope.launch {
            startLoading()
            val response: StockInfoResult =
                StockInfoRepo.getData(interval = infoInterval, symbol = selectedBank.stk!!)
            if (response.resultType == ResultType.SUCCESS) {
                selectedStockInfo.postValue(response)
                err.postValue("")
            } else {
                err.postValue("err: ${response.msg}")
            }
            endLoading()

        }
    }

    private fun startLoading() {
        loading.postValue(true)
    }

    private fun endLoading() {
        loading.postValue(false)
    }


}