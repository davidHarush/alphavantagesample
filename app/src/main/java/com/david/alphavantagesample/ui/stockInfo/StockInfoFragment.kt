package com.david.alphavantagesample.ui.stockInfo

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.david.alphavantagesample.R
import com.david.alphavantagesample.base.BaseFragment
import com.david.alphavantagesample.network.ResultType
import com.david.alphavantagesample.network.entities.TimeSeries
import com.david.alphavantagesample.util.gone
import com.david.alphavantagesample.util.visible
import kotlinx.android.synthetic.main.stock_info_fragment.*

class StockInfoFragment : BaseFragment(R.layout.stock_info_fragment) {

    companion object {
        fun newInstance() =
            StockInfoFragment()
    }

    private lateinit var mSelectedStockViewModel: SelectedStockViewModel


    override fun getFragmentName(): String = StockInfoFragment::class.java.simpleName

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let { fragmentActivity ->
            if (!::mSelectedStockViewModel.isInitialized) {
                mSelectedStockViewModel = ViewModelProvider(fragmentActivity)[SelectedStockViewModel::class.java]
                initInterval()
                mSelectedStockViewModel.runStockInfo()
                setObserves(fragmentActivity)
            }

        }
    }

    private fun initInterval() {
        radioGroup.check( R.id.min1)
        mSelectedStockViewModel.setInterval(SelectedStockViewModel.Interval_1min)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        reload.setOnClickListener {
            when (radioGroup.checkedRadioButtonId) {
                R.id.min1 -> mSelectedStockViewModel.setInterval(SelectedStockViewModel.Interval_1min)
                R.id.min5 -> mSelectedStockViewModel.setInterval(SelectedStockViewModel.Interval_5min)
                R.id.min15 -> mSelectedStockViewModel.setInterval(SelectedStockViewModel.Interval_15min)
                R.id.min30 -> mSelectedStockViewModel.setInterval(SelectedStockViewModel.Interval_30min)
                R.id.min60 -> mSelectedStockViewModel.setInterval(SelectedStockViewModel.Interval_60min)
            }
            mSelectedStockViewModel.runStockInfo()
        }
    }


    private fun setObserves(fragmentActivity: FragmentActivity) {
        mSelectedStockViewModel.getStockInfo()
            .observe(fragmentActivity,
                Observer { result ->
                    if(result.resultType == ResultType.SUCCESS) {
                        recyclerView?.adapter =
                            StockInfoAdapter(
                                data = ArrayList<Pair<String, TimeSeries>>(result.data)
                            )
                        recyclerView?.layoutManager =
                            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        radioGroup?.visible()
                        reload?.visible()
                        recyclerView?.visible()
                    }
                })

        mSelectedStockViewModel.getErr()
            .observe(fragmentActivity,
                Observer { result ->
                  if(result.isEmpty()){
                      recyclerView?.visible()
                  }else{
                      recyclerView?.gone()
                  }
                })

    }

}