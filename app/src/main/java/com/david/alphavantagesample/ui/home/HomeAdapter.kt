package com.david.alphavantagesample.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.david.alphavantagesample.R
import com.david.alphavantagesample.base.BaseHolder
import com.david.alphavantagesample.network.entities.BankItem
import com.david.alphavantagesample.util.getAppContext
import java.util.*


class HomeAdapter(
    private val data: ArrayList<BankItem>,
    private val callBack: CallBack
) : RecyclerView.Adapter<HomeAdapter.DataViewHolder>() {

    private val mInflater :LayoutInflater =  LayoutInflater.from(getAppContext())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        return DataViewHolder(mInflater.inflate(R.layout.stock_item, parent, false))
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
      holder.onBind(data[position])
    }

    inner class DataViewHolder(itemView: View) : BaseHolder(itemView) {
        private val mTitle: TextView
        private val mImg: ImageView

        init {
            itemView.setOnClickListener(this)
            mTitle = itemView.findViewById(R.id.title)
            mImg = itemView.findViewById(R.id.img)
        }

        override fun onBind(dataItem: Any) {
            val item = dataItem as BankItem
            itemView.tag = item
            mTitle.text = item.name
            mImg.load(item.img)

        }

        override fun onClick(v: View?) {
           callBack.onItemClick(itemView.tag as BankItem)
        }
    }

    interface CallBack {
        fun onItemClick(bankItem: BankItem)
    }

}