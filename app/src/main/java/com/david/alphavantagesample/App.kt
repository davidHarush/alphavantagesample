package com.david.alphavantagesample

import android.app.Application
import android.content.Context
import coil.Coil
import coil.ImageLoader
import com.david.alphavantagesample.util.getAppContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class App : Application(), CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.Main

    init {
        instance = this
    }

    companion object {
        var instance: App? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        launch { initCoil() }
    }

    private fun initCoil() {
        val imagePlaceHolder = R.mipmap.ic_launcher_round
        val imageLoader = ImageLoader(getAppContext()) {
            availableMemoryPercentage(0.5)
            bitmapPoolPercentage(0.5)
            error(imagePlaceHolder)
            placeholder(imagePlaceHolder)
            crossfade(true)
        }

        Coil.setDefaultImageLoader(imageLoader)

    }
}