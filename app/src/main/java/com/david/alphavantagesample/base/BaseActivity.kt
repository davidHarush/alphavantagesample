package com.david.alphavantagesample.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.david.alphavantagesample.util.LifecycleLog

abstract class BaseActivity : AppCompatActivity() {


    abstract fun getActivityName(): String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LifecycleLog(lifecycle = lifecycle, LifecycleOwnerName = getActivityName())
    }

}