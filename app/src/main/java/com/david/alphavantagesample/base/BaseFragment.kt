package com.david.alphavantagesample.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.david.alphavantagesample.util.LifecycleLog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


abstract class BaseFragment (layoutResID: Int): Fragment( layoutResID ) {
    private val job = Job()
    val uiScope = CoroutineScope(Dispatchers.Main + job)

    abstract fun getFragmentName(): String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LifecycleLog(lifecycle= lifecycle, LifecycleOwnerName = getFragmentName())

    }

    override fun onStop() {
        super.onStop()
        job.cancel()
    }
}