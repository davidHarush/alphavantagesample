package com.david.alphavantagesample.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseHolder(private val containerView: View) :
    RecyclerView.ViewHolder(containerView), View.OnClickListener {
    abstract fun onBind(dataItem : Any )

}
